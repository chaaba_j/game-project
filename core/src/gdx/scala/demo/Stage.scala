package gdx.scala.demo

import com.badlogic.gdx.math.Vector3
import gdx.scala.demo.datas._
import gdx.scala.demo.events.Event
import gdx.scala.demo.inputs.PlayerInput
import gdx.scala.demo.rendering.{RenderObject, RenderObjectFactory, Renderer}

import scala.reflect.ClassTag

sealed trait StageError {
  def message: String
}
case class InvalidObjectType(message: String) extends StageError
case class CannotReplace(message: String) extends StageError

trait ReadonlyGameEnvironment {
  protected def gameMap: GameMap

  def toMapIndex(x: Float, y: Float): MapPosition

  def readonlyGameMap: ReadonlyGameMap = gameMap

  def at(x: Float, y: Float): GameObject

  def at(id: Int): GameObject

  def getPlayers: Seq[Player]

  def renderObject(id: Int): Option[RenderObject]
}

class GameEnvironment(pgameMap: GameMap,
                      renderObjFactory: RenderObjectFactory,
                      private var players: Seq[Player],
                      renderer: Renderer) extends ReadonlyGameEnvironment {
  override protected val gameMap = pgameMap
  private val left = -(gameMap.width / 2f)
  private val top = -(gameMap.height / 2f)
  private val sampleObj = renderObjFactory.wall(new Vector3())
  private val scaleX = sampleObj.instance.transform.getScaleX
  private val scaleY = sampleObj.instance.transform.getScaleY

  def toMapIndex(x: Float, y: Float): MapPosition =
    MapPosition(Math.floor(x - left).toInt, Math.floor(y - top).toInt)

  def at(x: Float, y: Float): GameObject =
    gameMap.get(toMapIndex(x, y))

  def at(id: Int): GameObject =
    gameMap.get(id)

  def renderObject(id: Int): Option[RenderObject] =
    renderer.query(id)

  def add[A <: GameObject](x: Float, y: Float, obj: A, initialScale: Option[Float] = None)(implicit classTag: ClassTag[A]): Either[StageError, A] = {
    val mapPos = toMapIndex(x, y)
    val (centeredX, centeredY) = (mapPos.x + left + 0.5f, mapPos.y + top + 0.5f)
    val res = obj match {
      case bomb: Bomb =>
        val bombObj = renderObjFactory.bomb(
          new Vector3(centeredX, scaleY / 2f, centeredY), initialScale
        )
        Right(renderer.add(bombObj))
      case bonus: Bonus =>
        val bonusObj = renderObjFactory.bonus(
          bonus, new Vector3(centeredX, scaleY / 2f, centeredY), initialScale
        )
        Right(renderer.add(bonusObj))
      case malus : Malus =>
        val malusObj = renderObjFactory.malus(
          malus, new Vector3(centeredX, scaleY / 2f, centeredY), initialScale
        )
        Right(renderer.add(malusObj))
      case explosion: Explosion =>
        val explosionObj = renderObjFactory.explosion(
          new Vector3(centeredX, scaleY / 2f, centeredY), initialScale
        )
        Right(renderer.add(explosionObj))
      case _ =>
        Left(InvalidObjectType(s"cannot add type of class $classTag"))
    }
    res.map { id =>
      val objWithId = obj.setId(id)
      val mapPos = toMapIndex(x, y)
      println(s"add new object in the map $objWithId at ${mapPos.x}, ${mapPos.y}")
      gameMap.set(mapPos.x, mapPos.y, objWithId)
      objWithId.asInstanceOf[A]
    }
  }

  def replaceAt[A <: GameObject, B <: GameObject](a: A, b: B)(implicit classTag: ClassTag[B]): Either[StageError, B] =
    renderObject(a.id).map { aObj =>
      remove(a.id)
      add(aObj.translation.x, aObj.translation.z, b)
    }.getOrElse(Left(CannotReplace(s"cannot replace $a with $b")))

  def remove(id: Int): Unit = {
    val optRenderObj = renderer.query(id)
    optRenderObj.foreach { renderObj =>
      val pos = new Vector3()
      renderObj.instance.transform.getTranslation(pos)
      val mapPos = toMapIndex(pos.x, pos.z)
      gameMap.set(mapPos.x, mapPos.y, EmptyObject)
      renderer.remove(id)
    }
  }

  def resize(): Unit =
    renderer.resize()

  def removePlayer(id: Int): Unit =
    players = players.filter(_.id != id)

  def draw(): Unit =
    renderer.draw()

  def getPlayers: Seq[Player] = players

}

class Stage(environment: GameEnvironment,
            private var inputs: Array[PlayerInput]) {
  private var events: Stream[Event] = Stream()

  def update(): Unit = {
    if (inputs.length != environment.getPlayers.length)
      inputs = inputs.filter { input =>
        environment.getPlayers.contains(input.player)
      }
    events = Event.process(environment, events)
    events ++= inputs.flatMap { input =>
      input.update(input.poll())
    }
  }

  def isFinished: Boolean = inputs.isEmpty

  def resize(): Unit = environment.resize()

  def render(): Unit = environment.draw()
}
