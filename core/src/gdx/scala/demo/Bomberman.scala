package gdx.scala.demo

import com.badlogic.gdx.{ApplicationListener, Gdx}
import gdx.scala.demo.rendering._

class Bomberman extends ApplicationListener {

  private var stage: Stage = _

  val strMap =
    """|11111111111111
       |12220222222221
       |12212222110001
       |12020202020201
       |10222020022201
       |11010101010101
       |12010101010101
       |10020002020201
       |12012101010201
       |12012101010201
       |12012101010101
       |10012101212121
       |10000000000001
       |11111111111111""".stripMargin


  override def create(): Unit = {
    val lines = strMap.split("\n")
    lines.length
    stage = StageLoader.load(Theme.default, strMap)
  }

  override def dispose(): Unit = {
    StageLoader.clearAssets()
  }

  override def resize(width: Int, height: Int): Unit = {
    stage.resize()
  }

  override def pause(): Unit = {}

  override def render(): Unit = {
    if (stage.isFinished)
      Gdx.app.exit()
    stage.update()
    stage.render()
  }

  override def resume(): Unit = {}
}
