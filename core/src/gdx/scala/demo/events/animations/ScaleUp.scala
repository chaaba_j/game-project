package gdx.scala.demo.events.animations

import com.badlogic.gdx.math.Vector3
import gdx.scala.demo.{GameEnvironment, Stage}
import gdx.scala.demo.events.Event

case class ScaleUp(id: Int, toScale: Float, scaleBy: Float) extends Event {

  override def apply(stage: GameEnvironment): Stream[Event] = {
    println("scale up")
    stage.renderObject(id).map { renderObj =>
      val scale = new Vector3()
      renderObj.instance.transform.getScale(scale)
      if (scale.x < toScale) {
        renderObj.instance.transform.scale(scaleBy, scaleBy, scaleBy)
        Stream(ScaleUp(id, toScale, scaleBy))
      } else {
        Event.NoEvent
      }
    }.getOrElse(Event.NoEvent)
  }
}
