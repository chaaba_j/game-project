package gdx.scala.demo.events.animations

import com.badlogic.gdx.math.Vector3
import gdx.scala.demo.{GameEnvironment, Stage}
import gdx.scala.demo.events.Event

case class ScaleDown(id: Int, unscaleTo: Float, unscaleBy: Float) extends Event {
  override def apply(stage: GameEnvironment): Stream[Event] = {
    stage.renderObject(id).map { renderObj =>
      val scale = new Vector3()
      renderObj.instance.transform.getScale(scale)
      if (scale.x > unscaleBy) {
        renderObj.instance.transform.scale(unscaleBy, unscaleBy, unscaleBy)
        Stream(ScaleDown(id, unscaleTo, unscaleBy))
      } else {
        Event.NoEvent
      }
    }.getOrElse(Event.NoEvent)
  }
}
