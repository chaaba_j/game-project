package gdx.scala.demo.events

import java.util.logging.Logger

import gdx.scala.demo.{GameEnvironment, Stage}

trait Event {
  def apply(env: GameEnvironment): Stream[Event]
}

object EmptyEvent extends Event {
  override def apply(env: GameEnvironment): Stream[Event] =
    Stream()
}

object Event {
  val NoEvent = Stream.empty[Event]
  val logger = Logger.getLogger("events")

  def process(env: GameEnvironment, events: Stream[Event]): Stream[Event] = {
    events.foldLeft(Stream.empty[Event]) {
      case (acc, event) =>
        acc ++ event.apply(env)
    }
  }

}

