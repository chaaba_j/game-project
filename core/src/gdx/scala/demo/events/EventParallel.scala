package gdx.scala.demo.events
import gdx.scala.demo.{GameEnvironment, Stage}

case class EventParallel(events: Stream[Event]) extends Event {

  override def apply(stage: GameEnvironment): Stream[Event] = {
    val nextEvents = Event.process(stage, events)

    if (nextEvents.nonEmpty) {
      Stream(
        EventParallel(
          nextEvents
        )
      )
    } else {
      Event.NoEvent
    }
  }

}

object EventParallel {
  def apply(events: Event*): EventParallel =
    EventParallel(events.toStream)
}
