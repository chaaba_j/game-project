package gdx.scala.demo.events

import gdx.scala.demo.GameEnvironment
import gdx.scala.demo.datas.{Bonus, Malus}
import gdx.scala.demo.events.animations.ScaleUp

case class DropItem(x: Float, y: Float) extends Event {
  private val bonusLuck = 0.6

  override def apply(stage: GameEnvironment): Stream[Event] = {
    val luck = Math.random()
    if (luck <= bonusLuck) {
      stage.add(x, y, Bonus.random, Some(0.1f)) match {
        case Left(error) =>
          println(s"cannot add bonus $error")
          Event.NoEvent
        case Right(bonus) =>
          Stream(
            EventParallel(
              RotateItem(bonus),
              ScaleUp(bonus.id, 1.0f, 1.2f)
            )
          )
      }
    } else {
      stage.add(x, y, Malus.random, Some(0.1f)) match {
        case Left(error) =>
          println(s"cannot add malus: $error")
          Event.NoEvent
        case Right(malus) =>
          Stream(
            EventParallel(
              RotateItem(malus), ScaleUp(malus.id, 1.0f, 1.1f),
              ScaleUp(malus.id, 1.0f, 1.2f)
            )
          )
      }
    }
  }

}
