package gdx.scala.demo.events

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g3d.Attributes
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute
import com.badlogic.gdx.math.Vector3
import gdx.scala.demo.{GameEnvironment, Stage}
import gdx.scala.demo.datas.Explosion

case class ExplosionLife(explosion: Explosion, duration: Float) extends Event {
  override def apply(stage: GameEnvironment): Stream[Event] = {
    if (duration - Gdx.graphics.getDeltaTime > 0) {
      stage.renderObject(explosion.id).foreach { renderObj =>
        val scale = new Vector3()

        renderObj.instance.transform.getScale(scale)
        if (scale.x < 1f) {
          renderObj.instance.transform.scale(1.4f, 1.4f, 1.4f)
        }
        renderObj.instance.transform.rotate(0f, 1f, 0f, 0.5f)
      }
      Stream(
        ExplosionLife(
          explosion,
          duration - Gdx.graphics.getDeltaTime
        )
      ) ++ stage.getPlayers.map(PlayerCollide(_, explosion))
    }
    else {
      stage.renderObject(explosion.id).map { renderObj =>
        val scale = new Vector3()

        renderObj.instance.transform.getScale(scale)
        if (scale.x > 0.1f) {
          renderObj.instance.transform.scale(0.7f, 0.7f, 0.7f)
          renderObj.instance.transform.rotate(0f, 1f, 0f, 0.5f)
          Stream(ExplosionLife(explosion, duration))
        } else {
          stage.remove(explosion.id)
          Event.NoEvent
        }
      }.getOrElse(Event.NoEvent)
    }
  }
}
