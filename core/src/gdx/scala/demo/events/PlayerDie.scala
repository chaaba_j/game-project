package gdx.scala.demo.events

import gdx.scala.demo.{GameEnvironment, Stage}
import gdx.scala.demo.datas.Player
import gdx.scala.demo.events.animations.ScaleDown

case class PlayerDie(player: Player) extends Event {

  override def apply(stage: GameEnvironment): Stream[Event] = {
    stage.removePlayer(player.id)
    Stream(
      EventSequence(
        Stream(
          ScaleDown(player.id, 0.0000001f, 0.975f) ,
          RemoveRenderObject(player.id)
        )
      )
    )
  }
}
