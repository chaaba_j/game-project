package gdx.scala.demo.events

import gdx.scala.demo.{GameEnvironment, Stage}
import gdx.scala.demo.datas.GameObject

case class RotateItem(obj: GameObject) extends Event {

  override def apply(stage: GameEnvironment): Stream[Event] =
    stage.renderObject(obj.id).map { renderObj =>
      renderObj.instance.transform.rotate(-1f, -1f, -1f, 2f)
      Stream(RotateItem(obj))
    }.getOrElse(Event.NoEvent)

}
