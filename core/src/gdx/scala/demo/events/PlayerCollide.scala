package gdx.scala.demo.events

import com.badlogic.gdx.math.collision.BoundingBox
import gdx.scala.demo.{GameEnvironment, Stage}
import gdx.scala.demo.datas._

case class PlayerCollide(player: Player, entity: GameObject) extends Event {

  private def recentlyDroppedBomb(player: Player, bomb: Bomb): Boolean =
    player.lastBombDropped.exists(_.id == bomb.id)

  override def apply(stage: GameEnvironment): Stream[Event] = {
    val res = for {
      playerObj <- stage.renderObject(player.id)
      entityObj <- stage.renderObject(entity.id)
      events = {
        var playerBox = new BoundingBox()
        var entityBox = new BoundingBox()
        playerObj.instance.model.calculateBoundingBox(playerBox)
        entityObj.instance.model.calculateBoundingBox(entityBox)
        playerBox = playerBox.mul(playerObj.instance.transform)
        entityBox = entityBox.mul(entityObj.instance.transform)
        if (playerBox.intersects(entityBox) && !player.stats.phantomActivated) {
          entity match {
            case bomb: Bomb if recentlyDroppedBomb(player, bomb) =>
              Event.NoEvent
            case explosion: Explosion =>
              println("should die no ?")
              Stream(PlayerDie(player))
            case bonus: Bonus =>
              Stream(TakeBonus(player, bonus))
            case malus: Malus =>
              Stream(TakeMalus(player, malus))
            case x: GameObject =>
              playerObj.instance.transform.translate(0f, 0f, -player.stats.speed)
              Event.NoEvent
          }
        } else
          Event.NoEvent
      }
    } yield events
    res.getOrElse(Event.NoEvent)
  }

}
