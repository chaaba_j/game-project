package gdx.scala.demo.events

import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.math.collision.BoundingBox
import gdx.scala.demo.{GameEnvironment, Stage}
import gdx.scala.demo.datas.Direction.Direction
import gdx.scala.demo.datas._

case class Move(player: Player, direction: Direction) extends Event {

  def getAroundGameObject(stage: GameEnvironment, pos: Vector3): GameObject = {
    player.direction match {
      case Direction.Right =>
        stage.at(pos.x, pos.z)
      case Direction.Left =>
        stage.at(pos.x, pos.z)
      case Direction.Up =>
        stage.at(pos.x, pos.z)
      case Direction.Down =>
        stage.at(pos.x, pos.z)
    }
  }

  override def apply(stage: GameEnvironment): Stream[Event] = {
    val playerObj = stage.renderObject(player.id)

    playerObj.map { renderObj =>
      val box = new BoundingBox()
      val position = new Vector3()
      renderObj.instance.model.calculateBoundingBox(box)
      renderObj.instance.transform.translate(0f, 0f, player.stats.speed)
      renderObj.instance.transform.getTranslation(position)
      box.mul(renderObj.instance.transform)
      val entity = getAroundGameObject(stage, position)
      val events = if (entity != EmptyObject) {
        Stream(PlayerCollide(player, entity))
      }
      else if (player.lastBombDropped.exists(_.id != entity.id)) {
        player.lastBombDropped = None
        Event.NoEvent
      } else {
        Event.NoEvent
      }
      events ++ (
        if (player.direction != direction)
          Stream(Rotate(player, direction))
        else
          Event.NoEvent
      )
    }.getOrElse(Event.NoEvent)
  }
}
