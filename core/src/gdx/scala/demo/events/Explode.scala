package gdx.scala.demo.events

import com.badlogic.gdx.math.Vector3
import gdx.scala.demo.{GameEnvironment, Stage}
import gdx.scala.demo.datas._

case class Explode(bomb: Bomb) extends Event {

  private val duration = 1f

  private def addExplosion(stage: GameEnvironment, x: Float, y: Float): Option[Stream[Event]] = {
    val entity = stage.at(x, y)
    entity match {
      case Wall(_, false) => None
      case _: Bonus | _: Malus =>
        stage.add(x, y, Explosion(id = -1)) match {
          case Left(error) =>
            println(s"Cannot add explosion $error")
            Some(Event.NoEvent)
          case Right(explosion) =>
            Some(
              Stream(
                EntityExplode(entity),
                ExplosionLife(explosion, duration = duration)
              )
            )
        }
      case Wall(_, true)  =>
        stage.add(x, y, Explosion(id = -1)) match {
          case Left(error) =>
            println(s"Cannot add explosion $error")
            Some(Event.NoEvent)
          case Right(explosion) =>
            Some(
              Stream(
                EntityExplode(entity),
                EventSequence(
                  Stream(
                    ExplosionLife(explosion, duration = duration),
                    Wait(0.1f),
                    DropItem(x, y)
                  )
                )
              )
            )
        }
      case x: Bomb if x.id != bomb.id =>
        Some(Stream(Explode(x)))
      case _ =>
        stage.add(x, y, Explosion(id = -1)) match {
          case Left(error) =>
            println(s"Cannot add explosion $error")
            Some(Event.NoEvent)
          case Right(explosion) =>
            Some(Stream(ExplosionLife(explosion, duration = duration)))
        }
    }
  }

  private def propagate(stage: GameEnvironment, initPos: Vector3, range: Int, f: (Vector3, Int) => Vector3): Stream[Event] =
    Seq.range(1, range).foldLeft((Event.NoEvent, true)) {
      case ((acc, true), i) =>
        val pos = f(initPos, i)
        addExplosion(stage, pos.x, pos.z).map { explosionEvents =>
          (acc ++ explosionEvents, true)
        }.getOrElse((acc, false))
      case ((acc, false), _) => (acc, false)
    }._1


  override def apply(stage: GameEnvironment): Stream[Event] = {
    println("Explosion")
    val res = for {
      bombObj <- stage.renderObject(bomb.id)
      events = {
        val pos = bombObj.translation
        var eventBuffer = addExplosion(stage, pos.x, pos.z).getOrElse(Event.NoEvent)
        eventBuffer ++= propagate(stage, pos, bomb.range, (p, i) => new Vector3(p.x - i, p.y, p.z))
        eventBuffer ++= propagate(stage, pos, bomb.range, (p, i) => new Vector3(p.x + i, p.y, p.z))
        eventBuffer ++= propagate(stage, pos, bomb.range, (p, i) => new Vector3(p.x, p.y, p.z - i))
        eventBuffer ++= propagate(stage, pos, bomb.range, (p, i) => new Vector3(p.x, p.y, p.z + i))
        eventBuffer
      }
    } yield events
    stage.remove(bomb.id)
    res.getOrElse(Stream())
  }

}
