package gdx.scala.demo.events

import gdx.scala.demo.{GameEnvironment, Stage}
import gdx.scala.demo.datas.{Malus, Player}

case class TakeMalus(player: Player, malus: Malus) extends Event {
  override def apply(stage: GameEnvironment): Stream[Event] = {
    malus.apply(player)
    stage.remove(malus.id)
    Stream(ItemEffectLife(player, malus, malus.duration))
  }
}
