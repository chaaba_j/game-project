package gdx.scala.demo.events

import com.badlogic.gdx.Gdx
import gdx.scala.demo.{GameEnvironment, Stage}
import gdx.scala.demo.datas.{Duration, Item, Player}

case class ItemEffectLife(player: Player, item: Item, duration: Float) extends Event {
  override def apply(stage: GameEnvironment): Stream[Event] = {
    if (duration == Duration.Inf)
      Event.NoEvent
    else if (duration > 0)
      Stream(
        ItemEffectLife(
          player,
          item,
          duration - Gdx.graphics.getDeltaTime
        )
      )
    else {
      item.unapply(player)
      Event.NoEvent
    }
  }
}
