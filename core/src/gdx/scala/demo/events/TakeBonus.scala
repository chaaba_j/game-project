package gdx.scala.demo.events

import gdx.scala.demo.{GameEnvironment, Stage}
import gdx.scala.demo.datas.{Bonus, Player}

case class TakeBonus(player: Player, bonus: Bonus) extends Event {
  override def apply(stage: GameEnvironment): Stream[Event] = {
    bonus.apply(player)
    stage.remove(bonus.id)
    Stream(ItemEffectLife(player, bonus, bonus.duration))
  }
}
