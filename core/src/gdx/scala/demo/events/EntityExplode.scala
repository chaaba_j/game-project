package gdx.scala.demo.events

import gdx.scala.demo.{GameEnvironment, Stage}
import gdx.scala.demo.datas.GameObject

case class EntityExplode(entity: GameObject) extends Event {
  override def apply(stage: GameEnvironment): Stream[Event] = {
    println("Block explode")
    stage.remove(entity.id)
    Event.NoEvent
  }
}
