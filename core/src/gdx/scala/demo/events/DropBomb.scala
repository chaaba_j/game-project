package gdx.scala.demo.events

import com.badlogic.gdx.Game
import com.badlogic.gdx.math.Vector3
import gdx.scala.demo.{GameEnvironment, Stage}
import gdx.scala.demo.datas.{Bomb, EmptyObject, Player}
import gdx.scala.demo.events.animations.ScaleUp

case class DropBomb(player: Player) extends Event {
  override def apply(stage: GameEnvironment): Stream[Event] = {
    val bomb = Bomb(
      id = -1,
      range = player.stats.bombRange,
      duration = 3f
    )
    if (player.stats.currentDrop == player.stats.maxBombDrop)
      Event.NoEvent
    else {
      stage.renderObject(player.id).map { playerObj =>
        val position = new Vector3()
        playerObj.instance.transform.getTranslation(position)
        if (stage.at(position.x, position.z) == EmptyObject) {
          stage.add(position.x, position.z, bomb, Some(1 / 200f)) match {
            case Left(error) =>
              Event.logger.severe(s"Cannot add bomb at $position")
              Stream()
            case Right(newBomb: Bomb) =>
              player.stats.currentDrop += 1
              player.lastBombDropped = Some(newBomb)
              Stream(
                EventParallel(
                  BombLife(player, newBomb),
                  ScaleUp(newBomb.id, 1 / 200f, 1.5f)
                )
              )
            case _ =>
              Stream()
          }
        } else
          Stream()
      }.getOrElse(Stream())
    }
  }
}
