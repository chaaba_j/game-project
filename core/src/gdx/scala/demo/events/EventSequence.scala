package gdx.scala.demo.events
import gdx.scala.demo.{GameEnvironment, Stage}

import scala.collection.immutable.Stream.Empty

case class EventSequence(events: Stream[Event]) extends Event {
  override def apply(scene: GameEnvironment): Stream[Event] =
    if (events.nonEmpty) {
      val nextEvents = events.head.apply(scene)

      Stream(EventSequence(nextEvents ++ events.tail))
    } else {
      Empty
    }

}
