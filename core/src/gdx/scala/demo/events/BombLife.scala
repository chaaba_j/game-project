package gdx.scala.demo.events

import com.badlogic.gdx.Gdx
import gdx.scala.demo.{GameEnvironment, Stage}
import gdx.scala.demo.datas.{Bomb, Player}

case class BombLife(player: Player, bomb: Bomb) extends Event {

  override def apply(stage: GameEnvironment): Stream[Event] = {
    bomb.duration -= Gdx.graphics.getDeltaTime
    if (bomb.duration > 0) {
      Stream(BombLife(player, bomb))
    }
    else {
      player.stats.currentDrop -= 1
      Stream(Explode(bomb))
    }
  }

}
