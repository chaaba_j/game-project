package gdx.scala.demo.events

import gdx.scala.demo.{GameEnvironment, Stage}
import gdx.scala.demo.datas.Direction.Direction
import gdx.scala.demo.datas.{Direction, Player}

case class Rotate(player: Player, direction: Direction) extends Event {
  override def apply(stage: GameEnvironment): Stream[Event] = {
    stage.renderObject(player.id).map { playerObj =>
      playerObj.instance.transform.rotate(0, 0.5f, 0, -Direction.toDegrees(player.direction))
      playerObj.instance.transform.rotate(0, 0.5f, 0, Direction.toDegrees(direction))
    }
    player.direction = direction
    Stream()
  }
}
