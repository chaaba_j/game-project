package gdx.scala.demo.events
import gdx.scala.demo.{GameEnvironment, Stage}

case class RemoveRenderObject(id: Int) extends Event {
  override def apply(stage: GameEnvironment): Stream[Event] = {
    stage.remove(id)
    Event.NoEvent
  }
}
