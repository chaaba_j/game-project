package gdx.scala.demo.events
import com.badlogic.gdx.Gdx
import gdx.scala.demo.{GameEnvironment, Stage}

case class Wait(duration: Float) extends Event {
  override def apply(stage: GameEnvironment): Stream[Event] =
    if (duration > 0)
      Stream(Wait(duration - Gdx.graphics.getDeltaTime))
    else
      Event.NoEvent
}
