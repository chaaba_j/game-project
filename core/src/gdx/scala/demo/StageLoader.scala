package gdx.scala.demo

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.controllers.Controllers
import com.badlogic.gdx.graphics.PerspectiveCamera
import com.badlogic.gdx.math.Vector3
import gdx.scala.demo.datas.{Player, PlayerStats, Wall}
import gdx.scala.demo.inputs.{GamepadInput, IAInput, KeyboardInput, PlayerInput}
import gdx.scala.demo.parsers.GameMapParser
import gdx.scala.demo.rendering.{RenderObjectFactory, Renderer, Theme}

import scala.collection.JavaConverters._

object StageLoader {
  private lazy val assets: AssetManager = new AssetManager()

  type ThemeLoader = (AssetManager, Int, Int) => Theme

  def load(themeLoader: ThemeLoader, strMap: String): Stage = {
    val cam = new PerspectiveCamera(67f, Gdx.graphics.getWidth, Gdx.graphics.getHeight)
    cam.lookAt(-0.000f, -9.5f, -0.40f)
    cam.near = 1f
    cam.far = 300f
    cam.translate(0.19f, 10.815f, 1f)
    val gameMap = GameMapParser.parse(strMap)
    val renderer = new Renderer(cam)
    val theme = themeLoader(assets, gameMap.width, gameMap.height)
    val renderObjFactory = new RenderObjectFactory(
      themeLoader(assets, gameMap.width, gameMap.height)
    )
    val sampleWall = renderObjFactory.wall(new Vector3())
    val scaleY = sampleWall.instance.transform.getScaleY
    val scaleX = sampleWall.instance.transform.getScaleX
    val left = -(gameMap.width / 2f) + scaleX / 2f
    val top = -(gameMap.height / 2f) + scaleY / 2f

    gameMap.items.zipWithIndex.collect {
      case (Wall(id, isDestructible), index) =>
        val x = index % gameMap.width
        val y = index / gameMap.width
        if (isDestructible) {
          val wallObj = renderObjFactory.box(new Vector3(left + x, scaleY / 2f, top + y))
          renderer.set(id, wallObj)
        } else {
          val wallObj = renderObjFactory.wall(new Vector3(left + x, scaleY / 2f, top + y))
          renderer.set(id, wallObj)
        }
    }
    renderer.add(renderObjFactory.skybox)
    renderer.add(renderObjFactory.ground)
    val playerObj = renderObjFactory.player
    playerObj.translate(new Vector3(-1000f, 0f, 0f))
    val playerId = renderer.add(playerObj)
    val player2Id = renderer.add(renderObjFactory.player)
    val player = Player(
      id = playerId,
      name = "test",
      stats = PlayerStats(
        speed = 15f,
        bombRange = 2
      )
    )
    val player2 = Player(
      id = player2Id,
      name = "test2",
      stats = PlayerStats(
        speed = 15f,
        bombRange = 2
      )
    )
    val gameEnv = new GameEnvironment(gameMap, renderObjFactory, Seq(player, player2), renderer)
    val firstController = Controllers.getControllers.asScala.headOption
    val inputs = firstController.map { controller =>
      Array(
        new KeyboardInput(player),
        new GamepadInput(player2, controller)
      )
    }.getOrElse(
      Array[PlayerInput](
        new KeyboardInput(player),
        new IAInput(gameEnv,  player2)
      )
    )
    new Stage(gameEnv, inputs)
  }

  def clearAssets(): Unit =
    assets.dispose()
}
