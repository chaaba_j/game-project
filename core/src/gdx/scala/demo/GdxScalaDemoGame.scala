package gdx.scala.demo

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController
import com.badlogic.gdx.graphics.g3d._
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight
import com.badlogic.gdx.graphics.{FPSLogger, GL20, PerspectiveCamera}
import com.badlogic.gdx.{ApplicationAdapter, Gdx}
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.ModelInstance
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController

class GdxScalaDemoGame extends ApplicationAdapter {

  var cam: PerspectiveCamera = _
  var instances: Array[ModelInstance] = new Array()
  var batch: ModelBatch = _
  var environment: Environment = _
  var camController: CameraInputController = _
  var loading = true
  var assetManager: AssetManager = _
  var blocks: Array[ModelInstance] = new Array()
  var invaders: Array[ModelInstance] = new Array()
  var ship: ModelInstance = _
  var space: ModelInstance = _
  var fpsLogger: FPSLogger = _

  override def create(): Unit = {
    cam = new PerspectiveCamera(67f, Gdx.graphics.getWidth, Gdx.graphics.getHeight)
    cam.position.set(0, 7f, 10f)
    cam.lookAt(0, 0, 0)
    cam.near = 1f
    cam.far = 300f
    cam.update()

    camController = new CameraInputController(cam)
    Gdx.input.setInputProcessor(camController)
    batch = new ModelBatch()
    assetManager = new AssetManager()

    environment = new Environment
    environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f))
    environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f))
    assetManager.load("assets/ship.obj", classOf[Model])
    assetManager.load("assets/block.obj", classOf[Model])
    assetManager.load("assets/invader.obj", classOf[Model])
    assetManager.load("assets/spacesphere.obj", classOf[Model])
    fpsLogger = new FPSLogger
  }

  private def doneLoading(): Unit = {
    ship = new ModelInstance(assetManager.get("assets/ship.obj", classOf[Model]))
    ship.transform.setToRotation(Vector3.Y, 180).trn(0, 0, 6f)
    instances.add(ship)

    val blockModel = assetManager.get("assets/block.obj", classOf[Model])
    for (x <- -5f until 5f by 2) {
      val block = new ModelInstance(blockModel)
      block.transform.setToTranslation(x, 0, 3f)
      instances.add(block)
      blocks.add(block)
    }

    val invaderModel = assetManager.get("assets/invader.obj", classOf[Model])
    for (x <- -5f until 5f by 2) {
      for (z <- -8f until 0f by 2) {
        val invader = new ModelInstance(invaderModel)
        invader.transform.setToTranslation(x, 0, z)
        instances.add(invader)
        invaders.add(invader)
      }
    }

    space = new ModelInstance(assetManager.get("assets/spacesphere.obj", classOf[Model]))
    loading = false
  }

  override def render(): Unit = {
    if (loading && assetManager.update())
      doneLoading()
    camController.update()
    Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth, Gdx.graphics.getHeight)
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT)

    batch.begin(cam)
    batch.render(instances, environment)
    if (space != null)
      batch.render(space)
    batch.end()
    fpsLogger.log()
  }

  override def resize(width: Int, height: Int): Unit =
    super.resize(width, height)

  override def pause(): Unit =
    super.pause()

  override def resume(): Unit =
    super.resume()

  override def dispose(): Unit = {
    instances.clear()
    assetManager.dispose()
    batch.dispose()
  }
}