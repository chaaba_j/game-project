package gdx.scala.demo.inputs

import gdx.scala.demo.ReadonlyGameEnvironment
import gdx.scala.demo.datas.{ MapPosition, Player}
import gdx.scala.demo.ia.PathFinding
import gdx.scala.demo.inputs.PlayerAction.PlayerAction


class IAInput(stage: ReadonlyGameEnvironment, override val player: Player) extends PlayerInput {

  def nextMove(pos: MapPosition, pos2: MapPosition): Option[PlayerAction] =
    if (pos.x < pos2.x)
      Some(PlayerAction.Right)
    else if (pos.x > pos2.x)
      Some(PlayerAction.Left)
    else if (pos.y > pos2.y)
      Some(PlayerAction.Down)
    else if (pos.y < pos2.y)
      Some(PlayerAction.Up)
    else
      None

  override def poll(): Stream[PlayerAction] = {
    val move = for {
      enemy <- stage.getPlayers.find(_.id != player.id)
      enemyObj <- stage.renderObject(enemy.id)
      playerObj <- stage.renderObject(player.id)
      playerTranslation = playerObj.translation
      playerPos = stage.toMapIndex(playerTranslation.x, playerTranslation.z)
      enemyTranslation = enemyObj.translation
      enemyPos = stage.toMapIndex(enemyTranslation.x, enemyTranslation.z)
      nextBlock <- PathFinding.find(stage.readonlyGameMap, playerPos, enemyPos)
      nextMove <- nextMove(playerPos, nextBlock)
    } yield nextMove

    move.map(x => Stream(x)).getOrElse(Stream())
  }

}
