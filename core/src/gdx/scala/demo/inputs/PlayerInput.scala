package gdx.scala.demo.inputs

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.controllers.{Controller, ControllerListener, Controllers, PovDirection}
import com.badlogic.gdx.math.Vector3
import gdx.scala.demo.datas.{Direction, Player}
import gdx.scala.demo.events.{DropBomb, EmptyEvent, Event, Move}
import gdx.scala.demo.inputs.PlayerAction.PlayerAction

import scala.annotation.switch

object PlayerAction extends Enumeration {
  type PlayerAction = Value

  val Up = Value(0, "Up")
  val Down = Value(1, "Down")
  val Right = Value(2, "Right")
  val Left = Value(3, "Left")
  val Drop = Value(4, "Drop")
  val Pause = Value(5, "Pause")
}

trait PlayerInput {
  def player: Player
  def poll(): Stream[PlayerAction]
  def update(actions: Stream[PlayerAction]): Stream[Event] = {
    actions.foldLeft(Stream.empty[Event]) {
      case (acc, action) =>
        action match {
          case PlayerAction.Up =>
            acc :+ Move(player, Direction.Up)
          case PlayerAction.Down =>
            acc :+ Move(player, Direction.Down)
          case PlayerAction.Left =>
            acc :+ Move(player, Direction.Left)
          case PlayerAction.Right =>
            acc :+ Move(player, Direction.Right)
          case PlayerAction.Drop =>
            acc :+ DropBomb(player)
          case _ =>
            acc
        }
    }
  }

}

class KeyboardInput(override val player: Player) extends PlayerInput {

  private val mapping: Map[Int, PlayerAction] = Map(
    Keys.UP -> PlayerAction.Up,
    Keys.DOWN -> PlayerAction.Down,
    Keys.LEFT -> PlayerAction.Left,
    Keys.RIGHT -> PlayerAction.Right,
    Keys.X -> PlayerAction.Drop
  )
  private val directions =
    Seq(
      PlayerAction.Up,
      PlayerAction.Down,
      PlayerAction.Left,
      PlayerAction.Right
    )

  private def isDirection(playerAction: PlayerAction): Boolean =
    directions.contains(playerAction)

  private def hasAlreadyDirection(actions: Stream[PlayerAction]): Boolean =
    actions.exists(isDirection)

  override def poll(): Stream[PlayerAction] =
    mapping.foldLeft(Stream.empty[PlayerAction]) {
      case (acc, (keyCode, action)) if Gdx.input.isKeyPressed(keyCode) =>
        if (hasAlreadyDirection(acc) && isDirection(action))
          acc
        else
          acc :+ action
      case (acc, _) =>
        acc
    }

}

class GamepadInput(override val player: Player, controller: Controller) extends PlayerInput {

  override def poll(): Stream[PlayerAction] = {
    val povDir = controller.getPov(0)
    /*val action = povDir match {
      case PovDirection.north => Some(PlayerAction.Up)
      case PovDirection.south => Some(PlayerAction.Down)
      case PovDirection.east => Some(PlayerAction.Right)
      case PovDirection.west => Some(PlayerAction.Left)
      case PovDirection.southEast => Some(PlayerAction.Right)
      case PovDirection.northEast => Some(PlayerAction.Right)
      case PovDirection.southWest => Some(PlayerAction.Left)
      case PovDirection.northWest => Some(PlayerAction.Left)
      case _ => None
    }*/

    val action = (controller.getAxis(0), controller.getAxis(1)) match {
      case (_, axisY) if Math.abs(axisY) >= 0.5f =>
        if (axisY > 0) Some(PlayerAction.Down) else Some(PlayerAction.Up)
      case (axisX, _) if Math.abs(axisX) >= 0.5f =>
        if (axisX > 0) Some(PlayerAction.Right) else Some(PlayerAction.Left)
      case _ =>
        None
    }

    val dropPressed = if (controller.getButton(0)) Some(PlayerAction.Drop) else None
    Stream(action, dropPressed).filter(_.isDefined).map(_.get)
  }

}