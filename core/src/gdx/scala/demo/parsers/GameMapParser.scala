package gdx.scala.demo.parsers

import gdx.scala.demo.datas.{EmptyObject, GameMap, GameObject, Wall}

import scala.collection.mutable.ArrayBuffer

object GameObjectCodes {
  val box = '2'
  val wall = '1'
}

object GameMapParser {
  def parse(content: String): GameMap = {
    val lines = content.split("\n")
    val height = lines.length
    val width = lines.head.length
    val items = ArrayBuffer.fill[GameObject](width * height)(EmptyObject)
    var id = 0

    lines.flatten.zipWithIndex.foreach {
      case (code, index) => {
        code match {
          case GameObjectCodes.box =>
            items.update(index, Wall(id, isDestructible = true))
            id += 1
          case GameObjectCodes.wall =>
            items.update(index, Wall(id, isDestructible = false))
            id += 1
          case _ =>
            ()
        }
      }
    }
    GameMap(width, height, items.toArray)
  }
}
