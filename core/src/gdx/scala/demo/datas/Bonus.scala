package gdx.scala.demo.datas

object Duration {
  val Inf = -1
}

sealed trait Item extends GameObject {
  def apply(player: Player): Unit
  def unapply(player: Player): Unit
  def duration: Float = Duration.Inf
  override val traversable: Boolean = true
}

sealed trait Bonus extends Item

case class SpeedUp(id: Int, by: Float) extends Bonus {
  override def setId(id: Int): GameObject = copy(id = id)
  override def unapply(player: Player): Unit =
    player.stats.speed -= by
  override def apply(player: Player): Unit =
    player.stats.speed += by
}
case class RangeUp(id: Int, by: Int) extends Bonus {
  override def setId(id: Int): GameObject = copy(id = id)

  override def unapply(player: Player): Unit =
    player.stats.bombRange -= by
  override def apply(player: Player): Unit =
    player.stats.bombRange += by
}
case class BombUp(id: Int) extends Bonus {
  override def setId(id: Int): GameObject = copy(id = id)
  override def unapply(player: Player): Unit =
    player.stats.maxBombDrop -= 1
  override def apply(player: Player): Unit =
    player.stats.maxBombDrop += 1
}

case class Phantom(id: Int, time: Float) extends Bonus {
  override val duration: Float = 8f
  override def setId(id: Int): GameObject = copy(id = id)
  override def apply(player: Player): Unit =
    player.stats.phantomActivated = true
  override def unapply(player: Player): Unit =
    player.stats.phantomActivated = false
}

object Bonus {

  private val lucks = Seq(
    //(x: Double) => if (x > 0 && x <= ) Some(Phantom(-1, 5f): Bonus) else None,
    (x: Double) => if (x > 0 && x <= 30) Some(SpeedUp(-1, 5): Bonus) else None,
    (x: Double) => if (x > 30 && x <= 60) Some(RangeUp(-1, 1): Bonus) else None,
    (x: Double) => if (x > 60 && x <= 100) Some(BombUp(-1): Bonus) else None

  )

  def random: Bonus = {
    val rnd = Math.random() * 100
    lucks.find(luck => luck(rnd).nonEmpty).get(rnd).get
  }
}

sealed trait Malus extends Item
case class SpeedDown(id: Int, by: Float) extends Malus {
  override def apply(player: Player): Unit =
    if (player.stats.speed > 1)
      player.stats.speed -= by

  override def unapply(player: Player): Unit =
    if (player.stats.speed > 1)
      player.stats.speed += 1

  override def setId(id: Int): GameObject = copy(id = id)
}
case class RangeDown(id: Int, by: Int) extends Malus {
  override def apply(player: Player): Unit =
    if (player.stats.bombRange > 2)
      player.stats.bombRange -= by

  override def unapply(player: Player): Unit =
    if (player.stats.bombRange > 2)
      player.stats.bombRange += by

  override def setId(id: Int): GameObject = copy(id = id)
}

case class BombDown(id: Int) extends Malus {
  override def setId(id: Int): GameObject = copy(id = id)

  override def apply(player: Player): Unit =
    if (player.stats.maxBombDrop > 1)
      player.stats.maxBombDrop -= 1

  override def unapply(player: Player): Unit =
    if (player.stats.maxBombDrop > 1)
      player.stats.maxBombDrop += 1
}

object Malus {
  private val lucks = Seq(
    (x: Double) => if (x > 0 && x <= 30) Some(SpeedDown(-1, 5): Malus) else None,
    (x: Double) => if (x > 30 && x <= 60) Some(RangeDown(-1, 1): Malus) else None,
    (x: Double) => if (x > 60 && x <= 100) Some(BombDown(-1): Malus) else None
  )

  def random: Malus = {
    val rnd = Math.random() * 100
    lucks.find(luck => luck(rnd).nonEmpty).get(rnd).get
  }
}
