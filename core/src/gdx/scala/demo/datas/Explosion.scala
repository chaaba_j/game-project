package gdx.scala.demo.datas

case class Explosion(id: Int) extends GameObject {
  override def traversable: Boolean = false
  override def setId(id: Int): GameObject = copy(id = id)
}
