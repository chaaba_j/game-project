package gdx.scala.demo.datas

import gdx.scala.demo.datas.Direction.Direction

object Direction extends Enumeration {
  type Direction = Value

  val Up = Value(0, "Up")
  val Down = Value(1, "Down")
  val Right = Value(2, "Right")
  val Left = Value(3, "Left")

  def toDegrees(direction: Direction): Int =
    direction match {
      case Up => 360
      case Down => -180
      case Left => 90
      case Right => 270
    }
}

case class PlayerStats(
  var speed: Float,
  var bombRange: Int,
  var maxBombDrop: Int = 1,
  var currentDrop: Int = 0,
  var phantomActivated: Boolean = false
)

case class Player(
  id: Int,
  name: String,
  var direction: Direction = Direction.Down,
  var lastBombDropped: Option[Bomb] = None,
  stats: PlayerStats
) extends GameObject {
  override def setId(id: Int): GameObject = copy(id = id)
  override val traversable: Boolean = true
}