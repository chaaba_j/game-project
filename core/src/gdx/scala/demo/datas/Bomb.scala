package gdx.scala.demo.datas

case class Bomb(
  id: Int,
  range: Int,
  var duration: Float,
  var spawnTime: Float = 1f
) extends GameObject {
  override def setId(id: Int): GameObject = copy(id = id)
  override def traversable: Boolean = false
}
