package gdx.scala.demo.datas

object EmptyObject extends GameObject {
  val id = -1
  override def setId(id: Int): GameObject = this
  override val traversable: Boolean = true
}

case class MapPosition(x: Int, y: Int)

trait ReadonlyGameMap {
  def width: Int
  def height: Int
  def items: Array[GameObject]

  def get(pos: MapPosition): GameObject =
    get(pos.x, pos.y)

  def isEmpty(pos: MapPosition): Boolean =
    get(pos) == EmptyObject

  def isTraversable(pos: MapPosition): Boolean=
    get(pos).traversable

  def isDestructibleWall(pos: MapPosition): Boolean =
    get(pos) match {
      case Wall(_, true) => true
      case _ => false
    }

  def get(x: Int, y: Int): GameObject = {
    val index = x + (y * width)
    if (index >= 0 && index < items.length)
      items(index)
    else
      EmptyObject
  }

  def get(id: Int): GameObject =
    items.apply(id)
}

case class GameMap(
  width: Int,
  height: Int,
  items: Array[GameObject]
) extends ReadonlyGameMap {

  def set(x: Int, y: Int, obj: GameObject): Unit = {
    val index = x + (y * width)

    println(s"set $obj at $index $x $y")
    if (index >= 0 && index < items.length)
      items.update(index, obj)
  }

}
