package gdx.scala.demo.datas

trait GameObject {
  def id: Int
  def setId(id: Int): GameObject
  def traversable: Boolean
}
