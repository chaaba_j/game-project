package gdx.scala.demo.datas


case class Wall(
  id: Int,
  isDestructible: Boolean
) extends GameObject {
  override def setId(id: Int): GameObject = copy(id = id)
  override val traversable: Boolean = isDestructible
}