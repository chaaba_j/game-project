package gdx.scala.demo.ia

import gdx.scala.demo.datas._

private case class Around(
  up: MapPosition,
  down: MapPosition,
  left: MapPosition,
  right: MapPosition
)

case class PathNode(cost: Int, pos: MapPosition)

object PathFinding {
  private def nearestBlock(gameMap: ReadonlyGameMap, pos: MapPosition, pos2: MapPosition): Option[MapPosition] = {
    val left = pos.copy(x = pos.x - 1)
    val right = pos.copy(x = pos.x + 1)
    val up = pos.copy(y = pos.y + 1)
    val down = pos.copy(y = pos.y - 1)
    if (pos.x < pos2.x && gameMap.isTraversable(right))
      Some(right)
    else if (pos.x > pos2.x && gameMap.isTraversable(left))
      Some(left)
    else if (pos.y > pos2.y && gameMap.isTraversable(down))
      Some(up)
    else if (pos.y < pos2.y && gameMap.isTraversable(up))
      Some(down)
    else
      None
  }

  private def closestBlocks(gameMap: ReadonlyGameMap, pos: MapPosition): Around =
    Around(
      left = pos.copy(x = pos.x - 1),
      right = pos.copy(x = pos.x + 1),
      up = pos.copy(y = pos.y + 1),
      down = pos.copy(y = pos.y - 1)
    )

  def find(gameMap: ReadonlyGameMap, pos1: MapPosition, pos2: MapPosition): Option[MapPosition] = {
    val b = nearestBlock(gameMap, pos1, pos2)

    b
  }
  type Path = IndexedSeq[PathNode]

  def findPathRec(gameMap: ReadonlyGameMap, pos2: MapPosition, paths: IndexedSeq[Path]): Path = {
    val pathFound = paths.find(_.last.pos == pos2)
    if (paths.isEmpty)
      IndexedSeq()
    if (pathFound.nonEmpty)
      pathFound.get
    else {

    }
  }

  private def computeCost(gameObject: GameObject): Int =
    gameObject match  {
      case Wall(_, true) => 5
      case _ => 1
    }

  def findPath(gameMap: ReadonlyGameMap, pos1: MapPosition, pos2: MapPosition): Path = {
    var paths = IndexedSeq.empty[Path]
    val around = closestBlocks(gameMap, pos1)

    if (gameMap.isTraversable(around.up)) {
      val path = IndexedSeq(
        PathNode(computeCost(gameMap.get(around.right)), around.right)
      )
      paths = paths ++ IndexedSeq(path)
    }
    if (gameMap.isTraversable(around.down)) {
      val path = IndexedSeq(
        PathNode(computeCost(gameMap.get(around.down)), around.down)
      )
      paths = paths ++ IndexedSeq(path)
    }
    if (gameMap.isTraversable(around.left)) {
      val path = IndexedSeq(
        PathNode(computeCost(gameMap.get(around.left)), around.left)
      )
      paths = paths ++ IndexedSeq(path)
    }
    if (gameMap.isTraversable(around.right)) {
      val path = IndexedSeq(
        PathNode(computeCost(gameMap.get(around.right)), around.right)
      )
      paths = paths ++ IndexedSeq(path)
    }
    findPathRec(gameMap, pos2, paths)
  }
}

