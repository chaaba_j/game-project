package gdx.scala.demo.rendering

import java.util.concurrent.atomic.AtomicInteger

import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight
import com.badlogic.gdx.graphics.{Camera, FPSLogger, GL20, PerspectiveCamera}
import com.badlogic.gdx.graphics.g3d.{Environment, ModelBatch}
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController

import scala.collection.mutable

class Renderer(var cam: Camera) {

  var camController = new CameraInputController(cam)
  Gdx.input.setInputProcessor(camController)

  private val nextId: AtomicInteger = new AtomicInteger(0)
  private val fpsLogger = new FPSLogger
  private val batch: ModelBatch = new ModelBatch()
  private val env: Environment = new Environment
  private var instances: mutable.HashMap[Int, RenderObject] = mutable.HashMap.empty[Int, RenderObject]

  env.set(new ColorAttribute(ColorAttribute.Reflection, 0.4f, 0.4f, 0.4f, 1f))
  env.add(new DirectionalLight().set(1f, 1f, 1f, 1f, -0.8f, -0.2f))
  env.add(new DirectionalLight().set(1f, 1f, 1f, -1f, -0.8f, -0.2f))

  def add(obj: RenderObject): Int = {
    val id = nextId.addAndGet(1)
    instances.put(id, obj)
    id
  }

  def set(id: Int, obj: RenderObject): Int = {
    if (id > nextId.get()) {
      nextId.set(id)
      instances.put(id, obj)
      nextId.get()
    } else {
      instances.put(id, obj)
      id
    }
  }

  def remove(id: Int): Unit =
    instances.remove(id)

  def query(id: Int): Option[RenderObject] =
    instances.get(id)

  def resize(): Unit = {
    val oldCam = cam
    cam = new PerspectiveCamera(67f, Gdx.graphics.getWidth, Gdx.graphics.getHeight)
    cam.lookAt(oldCam.direction)
    cam.translate(oldCam.position)
    camController = new CameraInputController(cam)
    Gdx.input.setInputProcessor(camController)
  }

  def draw(): Unit = {
    val delta = Gdx.graphics.getDeltaTime
    cam.update()
    Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth, Gdx.graphics.getHeight)
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT
      | (if (Gdx.graphics.getBufferFormat.coverageSampling) GL20.GL_COVERAGE_BUFFER_BIT_NV
    else 0))
    batch.begin(cam)
    instances.foreach {
      case (_, obj) => obj.draw(batch, delta, env)
    }
    batch.end()
    fpsLogger.log()
  }
}