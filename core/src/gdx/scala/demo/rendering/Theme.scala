package gdx.scala.demo.rendering


import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.assets.loaders.TextureLoader.TextureParameter
import com.badlogic.gdx.graphics.Texture.TextureFilter
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.VertexAttributes.Usage
import com.badlogic.gdx.graphics.g3d.attributes.{BlendingAttribute, TextureAttribute}
import com.badlogic.gdx.graphics.g3d.{Material, Model}
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder
import gdx.scala.demo.datas._

trait Theme {
  def wallModel: Model
  def boxModel: Model
  def groundModel: Model
  def skyboxModel: Model
  def bombModel: Model
  def bonus(bonus: Bonus): Model
  def malus(malus: Malus): Model

  def playerModel: Model
  def explosionModel: Model

  def dispose(): Unit = {
    wallModel.dispose()
    groundModel.dispose()
  }
}

object Theme {


  val param: TextureParameter = new TextureParameter
  param.genMipMaps = true

  private def setTextureFilter(textures: Texture*): Unit = {
    textures.foreach { texture =>
      texture.setFilter(TextureFilter.MipMap, TextureFilter.Linear)
    }
  }

  private def createItemModel(texture: Texture): Model = {
    val modelBuilder = new ModelBuilder

    modelBuilder.createBox(
      0.5f, 0.5f, 0.01f,
      new Material(TextureAttribute.createDiffuse(texture), new BlendingAttribute(true, 0.6f)),
      Usage.Position | Usage.Normal | Usage.TextureCoordinates
    )
  }

  def default(assetManager: AssetManager, width: Int, height: Int): Theme = {
    val modelBuilder = new ModelBuilder
    val cubeTexturePath = "assets/textures/game/destBlock12.jpg"
    val boxTexturePath = "assets/textures/game/destBlock1.jpg"
    val groundTexturePath = "assets/textures/game/grass11.png"
    val skyboxTexturePath = "assets/textures/sky4.png"
    val playerModelPath = "assets/marvin.g3db"
    val bombModelPath = "assets/bomb.g3db"
    val explosionTexturePath = "assets/textures/gameExplosion.tga"
    val speedUpTexturePath = "assets/textures/bonus/speed++2.tga"
    val speedDownTexturePath = "assets/textures/bonus/speed--.tga"
    val rangeUpTexturePath = "assets/textures/bonus/fire++2.tga"
    val rangeDownTexturePath = "assets/textures/bonus/fire--.tga"
    val bombUpTexturePath = "assets/textures/bonus/bomb++2.tga"
    val bombDownTexturePath = "assets/textures/bonus/bomb--.tga"
    val phantomTexturepath = "assets/textures/bonus/wallpass.tga"
    assetManager.load(cubeTexturePath, classOf[Texture], param)
    assetManager.load(groundTexturePath, classOf[Texture], param)
    assetManager.load(skyboxTexturePath, classOf[Texture], param)
    assetManager.load(boxTexturePath, classOf[Texture], param)
    assetManager.load(explosionTexturePath, classOf[Texture], param)
    assetManager.load(speedUpTexturePath, classOf[Texture], param)
    assetManager.load(speedDownTexturePath, classOf[Texture], param)
    assetManager.load(rangeUpTexturePath, classOf[Texture], param)
    assetManager.load(rangeDownTexturePath, classOf[Texture], param)
    assetManager.load(bombUpTexturePath, classOf[Texture], param)
    assetManager.load(bombDownTexturePath, classOf[Texture], param)
    assetManager.load(phantomTexturepath, classOf[Texture], param)
    assetManager.load(playerModelPath, classOf[Model])
    assetManager.load(bombModelPath, classOf[Model])
    assetManager.finishLoading()
    val cubeTexture = assetManager.get(cubeTexturePath, classOf[Texture])
    val boxTexture = assetManager.get(boxTexturePath, classOf[Texture])
    val groundTexture = assetManager.get(groundTexturePath, classOf[Texture])
    val skyboxTexture = assetManager.get(skyboxTexturePath, classOf[Texture])
    val explosionTexture = assetManager.get(explosionTexturePath, classOf[Texture])
    val speedUpTexture = assetManager.get(speedUpTexturePath, classOf[Texture])
    val speedDownTexture = assetManager.get(speedDownTexturePath, classOf[Texture])
    val rangeUpTexture = assetManager.get(rangeUpTexturePath, classOf[Texture])
    val rangeDownTexture = assetManager.get(rangeDownTexturePath, classOf[Texture])
    val bombUpTexture = assetManager.get(bombUpTexturePath, classOf[Texture])
    val bombDownTexture = assetManager.get(bombDownTexturePath, classOf[Texture])
    val phantomTexture = assetManager.get(phantomTexturepath, classOf[Texture])
    setTextureFilter(
      cubeTexture, groundTexture, skyboxTexture,
      boxTexture, explosionTexture, speedUpTexture,
      speedDownTexture, rangeUpTexture, rangeDownTexture,
      bombUpTexture, bombDownTexture, phantomTexture
    )
    val cube = modelBuilder.createBox(
      1f, 1f, 1f,
      new Material(TextureAttribute.createDiffuse(cubeTexture)),
      Usage.Position | Usage.Normal | Usage.TextureCoordinates
    )
    val box = modelBuilder.createBox(
      1f, 1f, 1f,
      new Material(TextureAttribute.createDiffuse(boxTexture)),
      Usage.Position | Usage.Normal | Usage.TextureCoordinates
    )
    val ground = modelBuilder.createBox(
      width, 0.1f, height,
      new Material(TextureAttribute.createDiffuse(groundTexture)),
      Usage.Position | Usage.Normal | Usage.TextureCoordinates
    )
    val explosion = modelBuilder.createSphere(
      1f, 0.7f, 1f, 20, 20,
      new Material(TextureAttribute.createDiffuse(explosionTexture), new BlendingAttribute(true, 0.3f)),
      Usage.Position | Usage.Normal | Usage.TextureCoordinates
    )
    val skybox = modelBuilder.createSphere(
      100f, 100f, 100f, 20, 20,
      new Material(TextureAttribute.createDiffuse(skyboxTexture)),
      Usage.Position | Usage.Normal | Usage.TextureCoordinates
    )
    val speedUp = createItemModel(speedUpTexture)
    val speedDown = createItemModel(speedDownTexture)
    val rangeUp = createItemModel(rangeUpTexture)
    val rangeDown = createItemModel(rangeDownTexture)
    val bombUp = createItemModel(bombUpTexture)
    val bombDown = createItemModel(bombDownTexture)
    val phantom = createItemModel(phantomTexture)
    val player = assetManager.get(playerModelPath, classOf[Model])
    val bomb = assetManager.get(bombModelPath, classOf[Model])

    new Theme {
      override val wallModel: Model = cube

      override def groundModel: Model = ground

      override val skyboxModel: Model = skybox

      override val playerModel: Model = player

      override val boxModel: Model = box

      override val bombModel: Model = bomb

      override def explosionModel: Model = explosion

      override def bonus(bonus: Bonus): Model =
        bonus match {
          case x: SpeedUp => speedUp
          case x: RangeUp => rangeUp
          case x: BombUp => bombUp
          case x: Phantom => phantom
        }

      override def malus(malus: Malus): Model =
        malus match {
          case x: SpeedDown => speedDown
          case x: RangeDown => rangeDown
          case x: BombDown => bombDown
        }
    }
  }
}
