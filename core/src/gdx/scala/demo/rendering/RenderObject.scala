package gdx.scala.demo.rendering

import com.badlogic.gdx.graphics.g3d.utils.AnimationController
import com.badlogic.gdx.graphics.g3d.utils.AnimationController.AnimationListener
import com.badlogic.gdx.graphics.g3d.{Environment, Model, ModelBatch, ModelInstance}
import com.badlogic.gdx.math.Vector3
import gdx.scala.demo.datas._

class RenderObject(model: Model) {

  val instance = new ModelInstance(model)
  private val animationController = new AnimationController(instance)

  def setTranslation(vector3: Vector3): Unit =
    instance.transform.setTranslation(vector3)

  def translate(vector3: Vector3): Unit =
    instance.transform.translate(vector3)

  def translation: Vector3 = {
    val t = new Vector3()
    instance.transform.getTranslation(t)
    t
  }


  def animate(id: String, loopCount: Int, speed: Float): Unit =
    animationController.animate(id, 0.9f, 1.1f, loopCount, speed, new AnimationListener {
      override def onEnd(animation: AnimationController.AnimationDesc): Unit = println("end")

      override def onLoop(animation: AnimationController.AnimationDesc): Unit = println("looping")
    }, 0)

  def draw(batch: ModelBatch, delta: Float, env: Environment): Unit = {
    animationController.update(delta)
    batch.render(instance)
  }
}

class RenderObjectFactory(theme: Theme) {


  private def rescaleTo(renderObject: RenderObject, scale: Float): Unit =
    renderObject.instance.transform.scale(scale, scale, scale)

  def wall(pos: Vector3): RenderObject = {
    val cube = new RenderObject(theme.wallModel)
    cube.setTranslation(pos)
    cube
  }

  def box(pos: Vector3): RenderObject = {
    val box = new RenderObject(theme.boxModel)
    box.setTranslation(pos)
    box
  }

  def player: RenderObject = {
    val renderObj = new RenderObject(theme.playerModel)
    val scaling = 1f / 600f

    renderObj.instance.transform.translate(-0.5f, 0f, -0.2f)
    renderObj.instance.transform.scale(scaling, scaling, scaling)
    renderObj.animate("Take 001", -1, 1f)
    renderObj
  }


  def bomb(pos: Vector3, initialScale: Option[Float] = None): RenderObject = {
    val renderObject = new RenderObject(theme.bombModel)
    val scaling = 1f / 200f

    renderObject.instance.transform.translate(pos)
    renderObject.instance.transform.scale(scaling, scaling, scaling)
    initialScale.foreach(rescaleTo(renderObject, _))
    renderObject
  }

  def bonus(bonus: Bonus, pos: Vector3, initialScale: Option[Float] = None): RenderObject = {
    val renderObject = new RenderObject(theme.bonus(bonus))

    renderObject.instance.transform.translate(pos)
    initialScale.foreach(rescaleTo(renderObject, _))
    renderObject
  }

  def malus(malus: Malus, pos: Vector3, initialScale: Option[Float] = None): RenderObject = {
    val renderObject = new RenderObject(theme.malus(malus))

    renderObject.instance.transform.translate(pos)
    initialScale.foreach(rescaleTo(renderObject, _))
    renderObject
  }

  def skybox: RenderObject = {
    val skybox = new RenderObject(theme.skyboxModel)
    skybox.instance.transform.scale(-1f, -1f, -1f)
    skybox
  }

  def explosion(pos: Vector3, initialScale: Option[Float] = None): RenderObject = {
    val explosion = new RenderObject(theme.explosionModel)
    explosion.instance.transform.translate(pos)
    explosion.instance.transform.scale(0.1f, 0.1f, 0.1f)
    initialScale.foreach(rescaleTo(explosion, _))
    explosion
  }

  def ground: RenderObject =
    new RenderObject(theme.groundModel)

}

